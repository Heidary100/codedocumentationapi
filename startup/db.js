const mongoose = require('mongoose');
const config = require('config');

module.exports = function () {
    const dbConfig = config.get('db');

    // Connecting to Database
    mongoose.connect(dbConfig.connection, { useNewUrlParser: true })
        .then(() => console.log(`Connnected to ${dbConfig.connection} ...`))
        .catch(() => console.log('Failed connecting to db...'));

}