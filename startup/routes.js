const bodyParser = require('body-parser');
const { serverError, notFoundError } = require('../middlewares/error');

// const index = require('../services/route-services/index');
const index = require('../routes/index');

module.exports = function (app) {
    // route configs
    // body parser setup
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));

    // defining routes
    app.use('/api', index);


    // Error handling function
    app.use(serverError);
    app.use(notFoundError);
};