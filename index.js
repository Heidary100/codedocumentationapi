const express = require('express');
const app = express();
const port = process.env.PORT || 3333;

app.get('/', (req, res) => {
    res.send(`Node and express server is running on port ${port}`);
});

require('./startup/routes')(app);
require('./startup/db')();

// Initialization Server
const server = app.listen(port, () => console.log(`Listening on : http://localhost:${port}...`));

module.exports = server;