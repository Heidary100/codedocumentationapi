const {
    Post
} = require('../../models/Post');
const {
    Category
} = require('../../models/Category');
const {
    Author
} = require('../../models/Author');
const redis = require("redis"),
    redisClient = redis.createClient(process.env.REDIS_URL);

const elasticsearch = require('elasticsearch');
const elasticClient = new elasticsearch.Client({
    host: 'localhost:9200'
});

const util = require('util');
redisClient.get = util.promisify(redisClient.get);

function GetFromElastc(keyword, callBack = () => true) {
    elasticClient.search({
        index: 'posts',
        /// TODO: How to sort results by best match?
        q: `title:*${keyword}* OR text:*${keyword}*`
    }).then((res) => {
        let resultList = [];
        res.hits.hits.map((item) => {
            resultList.push(item._source);
        });
        callBack({
            success: true,
            response: resultList
        });
    }).catch((err) => {
        callBack({
            success: false,
            response: []
        });
    });
}

function StoreInElastic(post, callBack = () => true) {
    console.log('Indexing ' + post.id);
    elasticClient.index({
        index: 'posts',
        id: post['id'],
        //opType: 'create',
        type: 'post',
        body: {
            id: post['id'],
            text: post['text'],
            title: post['title']
        }
    }, (err, resp, status) => {
        let success = true;
        if (err)
            success = false;

        callBack({
            success: true,
            response: resp
        });
        console.log(resp.result);
    });
}

function IndexAllPosts(req, res) {
    Post.find({}, [], {
        sort: {
            order: 1 //Sort by Date Added DESC
        }
    }, (err, posts) => {
        posts.map((item) => {
            StoreInElastic(item);
        });

        res.send(`${posts.length} records were Indexed !`);
    });
}

function Search(req, res) {
    let searchKeyword = req.params.keyword;

    let query = {
        $or: [{
                title: {
                    $regex: searchKeyword,
                    $options: 'i'
                }
            },
            {
                text: {
                    $regex: searchKeyword,
                    $options: 'i'
                }
            }
        ]
    };
    GetFromElastc(searchKeyword, ({
        success,
        response
    }) => {
        console.log(`
        ${response.length} Records were Found: 
       ---------------------------------
       ${response} `);
        res.json(response);
    });
}

async function GetCategories(req, res) {
    try {
        const result = await redisClient.get('categories');

        if (result) return res.json(JSON.parse(result));

        StoreCategories(({
            success,
            result
        }) => {
            res.json(result);
        });
    } catch (err) {
        return res.send(err);
    }
}

async function GetPost(req, res) {
    let postId = req.params.id;
    console.log('Post Id:', postId);
    if (postId) {
        try {
            const result = await redisClient.get('post:' + postId);

            if (result) {
                console.log('ALREADY SAVED!');
                let post = JSON.parse(result);
                return res.send(post);
            }

            StorePost(postId, ({
                success,
                result
            }) => {
                console.log('SAVED!');
                res.json(result);
            });
        } catch (err) {
            return res.send(err);
        }
    };
}

function RemovePost(req, res) {
    let postId = req.params.id;

    redisClient.del('post:' + postId, (err) => {
        if (err) {
            res.send('Error');
        } else {
            res.send('SUCCESS');
        }
    });
}

function StoreCategories(callBack = () => true) {

    let newCategories = [];
    let success = false;

    Post.find({}, [], {
        sort: {
            order: 1 //Sort by Date Added DESC
        }
    }, (err, posts) => {

        if (err) {
            console.log(`An Error Occurred : 
                             ${err.message}`);
            posts = [];
            success = false;

            callBack({
                success: success,
                result: newCategories
            });
        } else {

            Category.find({}, [], {
                sort: {
                    order: 1 //Sort by Date Added DESC
                }
            }, (err2, categories) => {
                if (err2) {
                    console.log(`An Error Occurred : 
                             ${err2.message}`);
                    success = false;

                    callBack({
                        success: success,
                        result: newCategories
                    });
                } else {
                    categories.forEach((category) => {
                        let catPosts = [];
                        posts.forEach((post) => {
                            if (post.categoryId == category.id) {
                                catPosts.push({
                                    id: post.id,
                                    title: post.title
                                });
                            }
                        });

                        newCategories.push({
                            id: category.id,
                            name: category.title,
                            subjects: catPosts
                        });
                    });

                    redisClient.set('categories', JSON.stringify(newCategories));
                    console.log('REDIS: SAVED!');
                    success = true;
                    callBack({
                        success: success,
                        result: newCategories
                    });
                }
            });
        }
    });
}

function StorePost(postId, callBack = () => true) {
    let requestedPost = {};
    let success = false;
    if (postId) {
        Post.findById(postId, (err, post) => {
            if (!err && post) {
                requestedPost = {
                    id: post.id,
                    title: post.title,
                    text: post.text,
                    date: post.date,
                    author: ""
                };
                Author.findById(post.authorId, (err2, author) => {
                    if (!err2)
                        requestedPost = {
                            author: author.username
                        };

                    redisClient.set('post:' + postId, JSON.stringify(requestedPost), 'EX', (24 * 3600));
                    console.log('REDIS: SAVED!');
                    success = true;
                    callBack({
                        success: success,
                        result: requestedPost
                    });

                });
            } else {
                success = false;
                callBack({
                    success: success,
                    result: requestedPost
                });
            }
        });
    } else {
        success = false;
        callBack({
            success: success,
            result: requestedPost
        });
    }
}

module.exports = {
    GetCategories,
    GetPost,
    RemovePost,
    Search,
    IndexAllPosts,
    StoreInElastic,
    StoreCategories
};