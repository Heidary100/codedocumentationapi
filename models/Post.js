const mongoose = require('mongoose');

const postSchema = new mongoose.Schema({
    title: String,
    text: String,
    order: Number,
    date: Date,
    authorId: Number,
    categoryId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Category'
    }
});

postSchema.index({
    title: 'text',
    text: 'text'
});

const Post = mongoose.model('Post', postSchema)

module.exports = {
    Post
};