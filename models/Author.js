const mongoose = require('mongoose');

// Create author schema
let authorSchema = new mongoose.Schema({
        username:{
            type: String,
            required: true
        },
        password:{
            type: String,
            required: true
        }
});

const Author = mongoose.model('Author', authorSchema);

module.exports = {
    Author,
};