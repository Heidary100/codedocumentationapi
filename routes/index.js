const express = require('express');
// const { GetCategories, GetPost, Search, IndexAllPosts } = require('../services/index-service');
const {
    GetCategories,
    GetPost,
    Search,
    IndexAllPosts,
    RemovePost,
    StoreInElastic,
    StoreCategories
} = require('../services/route-services/index');

const router = express.Router();

router.get('/getCategories', GetCategories);
router.get('/getPost/:id?', GetPost);
router.get('/removePost/:id?', RemovePost);
router.get('/search/:keyword', Search);
router.get('/IndexAllPosts', IndexAllPosts);

router.post('/StoreInElastic', function (req, res) {
    let post = {
        id: req.body.id,
        title: req.body.title,
        text: req.body.text,
    };

    console.log(post);

    console.log('Indexing ' + post.id);

    StoreInElastic(post, ({success, response}) => {
        if(success)
            res.send('OK');
        else
            res.send('NO');
    });

});
router.get('/StoreCategories', (req, res) => {
    StoreCategories(({success, response}) => {
        if (success)
            res.send('OK');
        else
            res.send('No');
    });
});

module.exports = router;